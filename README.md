This software is developed as a universal aerospace engineering toolbox.
This software is still under development and also includes third-party codes.

If you want to contribute please head over to [Bitbucket](https://bitbucket.org/lukas-mueller/aeropy/src/master/) and 
create a fork.