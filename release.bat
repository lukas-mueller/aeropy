@ECHO off
setlocal
:PROMPT
SET /P AREYOUSURE=Are you sure (y/n)?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO END

python setup.py sdist
python setup.py bdist_wheel

:END
endlocal

pause