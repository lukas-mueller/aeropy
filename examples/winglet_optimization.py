#!/usr/bin/python
# -*- coding: utf-8 -*-


"""
Example of an winglet optimization using the AVL functions (take a look at the winglet analysis example before)
"""


import numpy as np
import scipy.optimize as sco
import matplotlib.pyplot as plt
import aeropy.atmosphere.model
import aeropy.aircraft.avl as avl


from winglet_helpers import avl_geometry, avl_analysis, avl_drag, avl_moment, avl_objective, avl_plot_geometry


# Flight conditions
mach_number = 0.6
altitude = 7000.
atmosphere = aeropy.atmosphere.model.ISA(altitude)
dynamic_pressure = 0.5*atmosphere.density()*(mach_number*atmosphere.a())**2

# Wing parameters
wing_params = {
    'wing_leading_edge_point_root': avl.Point(0., 0., 0.),
    'wing_leading_edge_point_tip': avl.Point(5., 15., 1.),
    'wing_chord_root': 6.,
    'wing_chord_tip': 1.5
}

# Winglet parameters
winglet_params = {
    'winglet_taper': 0.3,
    'winglet_leading_edge_sweep': 30.*np.pi/180.,
    'winglet_length': 0.07*15.*2,
    'winglet_chord_root': 0.75*1.5,
    'winglet_cant_angle': 70.*np.pi/180.,
    'winglet_twist_root': 0.,
    'winglet_twist_tip': 0.
}

# Analysis parameters
analysis_params = {
    'lift_coefficient': 0.5,
    'mach_number': mach_number,
    'density': atmosphere.density()
}

# Optimization parameters
k = 0.5


def minimization(x, CDind0, Mr0, k):
    """Setup the minimization function"""

    # Fist do analysis
    geometry = avl_geometry(mach_number=mach_number, winglet_taper=x[0], winglet_leading_edge_sweep=x[1],
                            winglet_length=x[2], winglet_chord_root=x[3], winglet_cant_angle=x[4],
                            winglet_twist_root=x[5], winglet_twist_tip=x[6], **wing_params)
    result = avl_analysis(geometry=geometry, **analysis_params)

    # Then do post-processing
    CDind = avl_drag(result)
    Mr = avl_moment(result, dynamic_pressure)

    # Finally calculate objective
    J = avl_objective(CDind, CDind0, Mr, Mr0, k)
    print(x)
    print(J)
    return J


def minimization_scaled(x_scaled, *args):
    """Setup the scaled minimization function (needed for better convergence)"""
    args = list(args)
    scale = args[-1]
    fun = args[-2]
    x = x_scaled*scale
    args = tuple(args[:-2])
    return fun(x, *args)


def minimize_scaled(fun, x0, args, bounds, method, options):
    """Setup the scaled minimize function (needed for better convergence)"""
    x0 = np.array(x0)
    scale = np.abs(x0)
    scale[scale == 0] = 1.
    x0_scaled = x0/scale
    bounds_scaled = [tuple(bound / scale[index] for bound in variable) for index, variable in enumerate(bounds)]
    args = args + (fun, scale)
    return sco.minimize(minimization_scaled, x0_scaled, args=args, bounds=bounds_scaled, method=method, options=options)


# Carry out baseline analysis for wing without winglet and do post-processing
geometry0 = avl_geometry(mach_number=mach_number, **wing_params, **winglet_params, winglet_on=False)
result0 = avl_analysis(geometry=geometry0, **analysis_params)
CDind0 = avl_drag(result0)
Mr0 = avl_moment(result0, dynamic_pressure)

# Do the same for the initial winglet config
geometry1 = avl_geometry(mach_number=mach_number, **wing_params, **winglet_params, winglet_on=True)
result1 = avl_analysis(geometry=geometry1, **analysis_params)
CDind1 = avl_drag(result1)
Mr1 = avl_moment(result1, dynamic_pressure)

# Now setup the optimization problem
x_0 = [
    winglet_params['winglet_taper'],
    winglet_params['winglet_leading_edge_sweep'],
    winglet_params['winglet_length'],
    winglet_params['winglet_chord_root'],
    winglet_params['winglet_cant_angle'],
    winglet_params['winglet_twist_root'],
    winglet_params['winglet_twist_tip']
]
x_lower = [
    0.4,
    0.,
    0.02*15.*2,
    0.4*1.5,
    10.*np.pi/180.,
    -6.,
    -6.
]
x_upper = [
    1.,
    45.*np.pi/180.,
    0.1*15.*2,
    1.*1.5,
    90.*np.pi/180.,
    6.,
    6.
]
bounds = list(zip(x_lower, x_upper))

# Finally run the optimization
optimum = minimize_scaled(minimization, x_0, args=(CDind0, Mr0, k), bounds=bounds, method='L-BFGS-B',
                          options={'eps': 1e-1})
random = sco.differential_evolution(minimization, list(zip(x_lower, x_upper)), args=(CDind0, Mr0, k),
                                    strategy='rand2exp')

# Plot initial wing
avl_plot_geometry(geometry0, name='initial wing without winglet')
# Plot initial wing & winglet
avl_plot_geometry(geometry1, name='initial wing with winglet')
# Plot cCl distribution
plt.figure()
plt.title(u'$c C_l$ comparison')
y0 = result0['Test']['StripForces']['Wing']['Yle']
y0 = y0[1:len(y0)//2]
cCl0 = result0['Test']['StripForces']['Wing']['c cl']
cCl0 = cCl0[1:len(cCl0)//2]
plt.plot(y0, cCl0, label='initial wing without winglet')
#y1_wing = result1['Test']['StripForces']['Wing']['Yle']
#y1_wing = y1[1:len(y1)//2]
#cCl1 = result1['Test']['StripForces']['Wing']['c cl']
#cCl1 = cCl1[1:len(cCl1)//2]
#plt.plot(y1, cCl1, label='initial wing with winglet')
plt.xlabel('$y$ (in m)')
plt.ylabel(u'$c C_l$')
plt.legend()
plt.show()