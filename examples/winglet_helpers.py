#!/usr/bin/python
# -*- coding: utf-8 -*-


"""
Helper functions for the example winglet analysis and winglet optimization examples using the AVL functions
"""


import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as sci
import aeropy.aircraft.avl as avl

from mpl_toolkits.mplot3d import Axes3D


def avl_geometry(mach_number,
                 wing_leading_edge_point_root,
                 wing_leading_edge_point_tip,
                 wing_chord_tip,
                 wing_chord_root,
                 winglet_taper,
                 winglet_leading_edge_sweep,
                 winglet_length,
                 winglet_chord_root,
                 winglet_cant_angle,
                 winglet_twist_root,
                 winglet_twist_tip,
                 winglet_on=True):
    """Create the AVL geometry object representing a generic wing with winglet"""

    # Firstly, calculate additional wing parameters
    wing_span = (wing_leading_edge_point_tip.y - wing_leading_edge_point_root.y) * 2
    wing_taper = wing_chord_tip / wing_chord_root

    # Secondly, calculate additional winglet parameters
    winglet_span = np.sin(winglet_cant_angle)*winglet_length
    winglet_chord_tip = winglet_chord_root*winglet_taper
    winglet_leading_edge_point_root = avl.Point(wing_leading_edge_point_tip.x+wing_chord_tip-winglet_chord_root,
                                                wing_leading_edge_point_tip.y,
                                                wing_leading_edge_point_tip.z)
    winglet_leading_edge_point_tip = avl.Point(winglet_leading_edge_point_root.x+np.tan(winglet_leading_edge_sweep) *
                                               winglet_span,
                                               winglet_leading_edge_point_root.y+winglet_span,
                                               winglet_leading_edge_point_root.z+np.cos(winglet_cant_angle) *
                                               winglet_length)
    reference_area = wing_span*(wing_chord_root+wing_chord_tip)/2.
    reference_chord = wing_chord_root*2./3.*((1.+wing_taper + wing_taper**2.)/(1.+wing_taper))
    reference_span = wing_span

    # Thirdly, create the surface objects
    wing_root_section = avl.Section(leading_edge_point=wing_leading_edge_point_root,
                                    chord=wing_chord_root)
    wing_tip_section = avl.Section(leading_edge_point=wing_leading_edge_point_tip,
                                   chord=wing_chord_tip)
    wing_surface = avl.Surface(name='Wing',
                               n_chordwise=15,
                               chord_spacing=avl.Spacing.cosine,
                               n_spanwise=30,
                               span_spacing=avl.Spacing.cosine,
                               y_duplicate=0.0,
                               component=1,
                               sections=[wing_root_section, wing_tip_section])

    winglet_root_section = avl.Section(leading_edge_point=winglet_leading_edge_point_root,
                                       chord=winglet_chord_root,
                                       angle=winglet_twist_root)
    winglet_tip_section = avl.Section(leading_edge_point=winglet_leading_edge_point_tip,
                                      chord=winglet_chord_tip,
                                      angle=winglet_twist_tip)
    winglet_surface = avl.Surface(name='Winglet',
                                  n_chordwise=10,
                                  chord_spacing=avl.Spacing.cosine,
                                  n_spanwise=15,
                                  span_spacing=avl.Spacing.cosine,
                                  y_duplicate=0.0,
                                  component=1,
                                  sections=[winglet_root_section, winglet_tip_section])

    # Fourthly, create the geometry object
    if winglet_on:
        surfaces = [wing_surface, winglet_surface]
    else:
        surfaces = [wing_surface]
    geometry = avl.Geometry(name='Model',
                            mach=mach_number,
                            reference_area=reference_area,
                            reference_chord=reference_chord,
                            reference_span=reference_span,
                            reference_point=avl.Point(0., 0., 0.),
                            surfaces=surfaces)
    return geometry


def avl_analysis(geometry, lift_coefficient, mach_number, density):
    """Create and run the ALV analysis"""

    # Please note that density, velocity, gravitational acceleration, etc. play no role in this analysis
    # (only for other ALV calculations regarding stability)
    control = avl.Parameter(name='alpha', constraint='CL', value=lift_coefficient)
    case = avl.Case(name='Test', alpha=control, Mach=mach_number, density=density)
    session = avl.Session(geometry=geometry, cases=[case])
    # Run and get results
    results = session.get_results()
    return results


def avl_drag(result):
    """Retrieve induced drag from AVL result dictionary"""

    # This is a simply dictionary lookup
    CDind = result['Test']['Totals']['CDind']
    return CDind


def avl_moment(result, dynamic_pressure):
    """Retrieve bending moment by integration of result from ALV"""

    # Get sectional data for half wing (thus split function is used)
    Yles = [np.split(np.array(surface['Yle']), 2)[0] for surface_key, surface in result['Test']['StripForces'].items()]
    cCls = [np.split(np.array(surface['c cl']), 2)[0] for surface_key, surface in result['Test']['StripForces'].items()]

    # Combine sectional data and inverse order for integration (thus the flip function is used)
    Yle = np.flip(np.concatenate(Yles))
    cCl = np.flip(np.concatenate(cCls))

    # Calculate sectional shear forces
    S = sci.cumtrapz(cCl * dynamic_pressure, Yle, initial=0)

    # Finally calculate the root bending moment
    Mr = np.trapz(S, Yle)
    return Mr


def avl_objective(CDind, CDind0, Mr, Mr0, k):
    """Setup the objective function for evaluation of the winglet performance"""

    J = k*CDind/CDind0+(1.-k)*(Mr/Mr0)
    return J


def avl_plot_geometry(geometry, name=''):
    """Plot the geometry of the wing and winglet"""

    # Iso view
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.set_title('Isometric view ' + name)
    for surface in geometry.surfaces:
        ax.plot(*surface.contour(), label=surface.name)
    ax.set_xlabel(u'$x$ (in m)')
    ax.set_ylabel(u'$y$ (in m)')
    ax.set_zlabel(u'$z$ (in m)')
    ax.legend()
    plt.show()

    # Top view
    plt.figure()
    plt.title('Top view '+name)
    for surface in geometry.surfaces:
        x, y, z = surface.contour()
        plt.plot(x, y, label=surface.name)
    plt.xlabel(u'$x$ (in m)')
    plt.ylabel(u'$y$ (in m)')
    plt.legend()
    plt.show()

    # Side view
    plt.figure()
    plt.title('Side view '+name)
    for surface in geometry.surfaces:
        x, y, z = surface.contour()
        plt.plot(x, z, label=surface.name)
    plt.xlabel(u'$x$ (in m)')
    plt.ylabel(u'$z$ (in m)')
    plt.legend()
    plt.show()

    # Front view
    plt.figure()
    plt.title('Front view '+name)
    for surface in geometry.surfaces:
        x, y, z = surface.contour()
        plt.plot(y, z, label=surface.name)
    plt.xlabel(u'$y$ (in m)')
    plt.ylabel(u'$z$ (in m)')
    plt.legend()
    plt.show()

    return
