#!/usr/bin/python
# -*- coding: utf-8 -*-


"""
Example of the airfoil shape generator
"""


import numpy as np
import matplotlib.pyplot as plt
import aeropy.airfoils.shapes


# CST shape
coef_upper = np.array([0.1853, 0.0744, 0.2011, 0.1082, 0.3499])
coef_lower = np.array([-0.1986, -0.0459, -0.2527, -0.1783, 0.3355])
x, y = aeropy.airfoils.shapes.cst(coef_upper, coef_lower, 149)
plt.figure()
plt.title('CST (arbitrary) airfoil shape')
plt.xlabel('x')
plt.ylabel('y')
plt.plot(x, y)
plt.show()


# NACA shape
x, y = aeropy.airfoils.shapes.naca('2414', 149, half_cosine_spacing=False)
plt.figure()
plt.title('NACA2412 airfoil shape')
plt.xlabel('x')
plt.ylabel('y')
plt.plot(x, y)
plt.show()


# Database shapes
x1, y1 = aeropy.airfoils.shapes.database('b737a')
x2, y2 = aeropy.airfoils.shapes.database('b737b')
x3, y3 = aeropy.airfoils.shapes.database('b737c')
plt.figure()
plt.title('B737 airfoil shapes')
plt.xlabel('x')
plt.ylabel('y')
plt.plot(x1, y1, label='root section')
plt.plot(x2, y2, label='middle section')
plt.plot(x3, y3, label='tip section')
plt.legend()
plt.show()