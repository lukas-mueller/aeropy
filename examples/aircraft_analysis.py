#!/usr/bin/python
# -*- coding: utf-8 -*-


"""
Example of the aircraft analysis using the AVL tool
Taken from https://github.com/renoelmendorp/AVLWrapper
"""


import aeropy.aircraft.avl as avl
import aeropy.airfoils.shapes as shp


# First setup the geometry of the aircraft
flap = avl.Control(name='Flap',
                   gain=1.0,
                   x_hinge=0.8,
                   duplicate_sign=1)
wing_root_section = avl.Section(leading_edge_point=avl.Point(0, 0, 0),
                                chord=1.0,
                                controls=[flap],
                                airfoil=avl.Airfoil(shp.naca('2414')))
wing_tip_section = avl.Section(leading_edge_point=avl.Point(0.6, 2.0, 0),
                               chord=0.4,
                               controls=[flap],
                               airfoil=avl.Airfoil(shp.naca('2410')))
wing_surface = avl.Surface(name='Wing',
                           n_chordwise=8,
                           chord_spacing=avl.Spacing.cosine,
                           n_spanwise=12,
                           span_spacing=avl.Spacing.cosine,
                           y_duplicate=0.0,
                           sections=[wing_root_section, wing_tip_section])
elevator = avl.Control(name='Elevator',
                       gain=1.0,
                       x_hinge=0.6,
                       duplicate_sign=1)
tail_root_section = avl.Section(leading_edge_point=avl.Point(3.5, 0, 0.2),
                                chord=0.4,
                                controls=[elevator])
tail_tip_section = avl.Section(leading_edge_point=avl.Point(3.7, 1.2, 0.2),
                               chord=0.25,
                               controls=[elevator])
tail_surface = avl.Surface(name='Horizontal Stabiliser',
                           n_chordwise=8,
                           chord_spacing=avl.Spacing.cosine,
                           n_spanwise=8,
                           span_spacing=avl.Spacing.cosine,
                           y_duplicate=0.0,
                           sections=[tail_root_section, tail_tip_section])
geometry = avl.Geometry(name='Test Aircraft',
                        reference_area=4.8,
                        reference_chord=0.74,
                        reference_span=4,
                        reference_point=avl.Point(0.21, 0, 0.15),
                        surfaces=[wing_surface, tail_surface])

# Then setup the test cases
# Case defined by one angle-of-attack
cruise_case = avl.Case(name='Cruise', alpha=4.0)
# More elaborate case, angle-of-attack of 4deg, elevator parameter which sets Cm (pitching moment) to 0.0
control_param = avl.Parameter(name='elevator',
                              constraint='Cm',
                              value=0.0)
cruise_trim_case = avl.Case(name='Trimmed',
                            alpha=4.0,
                            elevator=control_param)
# Landing case, flaps down by 15deg
landing_case = avl.Case(name='Landing', alpha=7.0, flap=15.0)

# Finally create a session with the geometry object and the cases
session = avl.Session(geometry=geometry, cases=[cruise_case, cruise_trim_case, landing_case])

# Show the geometry with AVL
session.show_geometry()
session.show_trefftz_plot(1)

# Print results
results = session.get_results()
print(results)
