#!/usr/bin/python
# -*- coding: utf-8 -*-


"""
Example of an winglet analysis and optimization using the AVL functions
"""


import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import aeropy.atmosphere.model
import aeropy.aircraft.avl as avl

from winglet_helpers import avl_geometry, avl_analysis, avl_drag, avl_moment, avl_objective


# Flight conditions
mach_number = 0.6
altitude = 7000.
atmosphere = aeropy.atmosphere.model.ISA(altitude)
dynamic_pressure = 0.5*atmosphere.density()*(mach_number*atmosphere.a())**2

# Wing parameters
wing_params = {
    'wing_leading_edge_point_root': avl.Point(0., 0., 0.),
    'wing_leading_edge_point_tip': avl.Point(5., 15., 1.),
    'wing_chord_root': 6.,
    'wing_chord_tip': 1.5
}

# Winglet parameters
winglet_params = {
    'winglet_taper': 0.3,
    'winglet_leading_edge_sweep': 30.*np.pi/180.,
    'winglet_length': 0.07*15.*2,
    'winglet_chord_root': 0.75*1.5,
    'winglet_cant_angle': 70.*np.pi/180.,
    'winglet_twist_root': 0.,
    'winglet_twist_tip': 0.
}

# Analysis parameters
analysis_params = {
    'lift_coefficient': 0.5,
    'mach_number': mach_number,
    'density': atmosphere.density()
}

# Carry out baseline analysis
geometry = avl_geometry(mach_number=mach_number, **wing_params, **winglet_params, winglet_on=False)
result = avl_analysis(geometry=geometry, **analysis_params)

# Carry out winglet analysis for different cant angles
winglet_cant_angles = np.linspace(0, 90, 11)*np.pi/180.
geometries = []
results = []
for winglet_cant_angle in winglet_cant_angles:
    winglet_params['winglet_cant_angle'] = winglet_cant_angle
    geometries.append(avl_geometry(mach_number=mach_number, **wing_params, **winglet_params))
    results.append(avl_analysis(geometry=geometries[-1], **analysis_params))

# Carry out post-processing of analyses
CDind0 = avl_drag(result)
CDinds = np.array([avl_drag(result) for result in results])
Mr0 = avl_moment(result, dynamic_pressure)
Mrs = np.array([avl_moment(result, dynamic_pressure) for result in results])

# Plot induced drag coefficient
plt.figure()
plt.title('Induced drag coefficient for different winglet geometries')
plt.xlabel(u'cant angle $\\phi$ (in deg)')
plt.ylabel(u'induced drag coefficient $C_{D_i}$')
plt.axhline(y=CDind0, color='r', label='wing without winglet')
plt.plot(winglet_cant_angles/np.pi*180., CDinds, label='wing with winglet')
plt.legend()
plt.show()

# Plot root bending moment
plt.figure()
plt.title('Root bending moment for different winglet geometries')
plt.xlabel(u'cant angle $\\phi$ (in deg)')
plt.ylabel(u'root bending moment $M_r$ (in Nm)')
plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
plt.axhline(y=Mr0, color='r', label='wing without winglet')
plt.plot(winglet_cant_angles/np.pi*180., Mrs, label='wing with winglet')
plt.legend()
plt.show()

# Plot relative effects on induced drag and root bending moment
plt.figure()
plt.title('Relative effects for different winglet geometries')
plt.xlabel(u'cant angle $\\phi$ (in deg)')
plt.ylabel('ratio')
plt.plot(winglet_cant_angles/np.pi*180., CDinds/CDind0-1, label=u'$\\frac{C_{D_i}}{C_{D_{i_0}}}-1$')
plt.plot(winglet_cant_angles/np.pi*180., Mrs/Mr0-1, label=u'$\\frac{M_{r}}{M_{r_0}}-1$')
plt.legend()
plt.show()

# Carry out some more postprocessing (sensitivity analysis of k)
ks = np.linspace(0, 1, 101)
Js = np.array([avl_objective(CDinds, CDind0, Mrs, Mr0, k) for k in ks])

# Plot heatmap showing objective function results for variations of both the cant angle and k (sensitivity analysis)
plt.figure()
plt.title('Effect on the objective function')
df = pd.DataFrame(Js, index=np.round(ks, 2), columns=winglet_cant_angles/np.pi*180.)
ax = sns.heatmap(df, yticklabels=20, xticklabels=2)
ax.invert_yaxis()
plt.xlabel(u'cant angle $\\phi$ (in deg)')
plt.ylabel(u'weighting factor $k$')
plt.show()
