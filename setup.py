from setuptools import setup, find_packages


version = '0.2'


def readme():
    with open('README.md') as f:
        return f.read()


setup(name='aeropy',
      version=version,
      python_requires='>=3',
      description='An universal aerospace engineering toolbox',
      long_description=readme(),
      long_description_content_type="text/markdown",
      classifiers=[
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: GPL-3.0',
        'Programming Language :: Python :: 3.7',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering',
      ],
      keywords='aerospace engineering airfoils noise aircraft',
      url='https://bitbucket.org/lukas-mueller/aeropy',
      download_url='https://bitbucket.org/lukas-mueller/aeropy/raw/master/dist/aeropy-'+version+'.tar.gz',
      author='Lukas Mueller',
      author_email='lukas.mueller94@gmail.com',
      license='GNU General Public License v3.0',
      packages=find_packages(),
      install_requires=[
            'matplotlib',
            'numpy',
            'pandas',
            'scipy',
            'seaborn'
      ],
      include_package_data=True,
      zip_safe=False)
