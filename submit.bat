@ECHO off
setlocal
:PROMPT
SET /P AREYOUSURE=Are you sure (y/n)?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO END

REM python setup.py register -r pypi
REM python setup.py sdist upload -r pypi

REM python -m twine register dist/*
python -m twine upload dist/*

:END
endlocal

pause