The following functions are beeing implemented in near future:

aeropy/airfoils/shapes:
- add airfoil archieve
- add PARSEC generator

aeropy/aerodynamics:
- add isentropic flow module (similar to https://github.com/AeroPython/scikit-aero/tree/master/skaero)
- add shock module (similar to https://github.com/AeroPython/scikit-aero/tree/master/skaero)
- add nozzle module (similar to https://github.com/AeroPython/scikit-aero/tree/master/skaero)

aeropy/aircraft/corrections:
- add airspeed corrections

tests:
- add verification test